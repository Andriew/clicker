﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;

namespace clicker
{
    public partial class Form1 : Form
    {
        private bool run;
        private bool _shouldStop;
        private Thread thread;
        private readonly WinHooksNET.KeyboardHook _keyboardHook = new WinHooksNET.KeyboardHook();
        
        public Form1()
        {
            InitializeComponent();
            run = false;
            _keyboardHook.OnKeyboardAction += _keyboardHook_OnKeyboardAction;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        public void DoMouseClick()
        {

            int time = Int32.Parse(this.textBox1.Text);
            
            _shouldStop = false;
            while (!_shouldStop)
            {
                //Call the imported function with the cursor's current position
                uint X = (uint)Cursor.Position.X;
                uint Y = (uint)Cursor.Position.Y;
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, X, Y, 0, 0);
                System.Threading.Thread.Sleep( time );
            }
        }
        
        public void _keyboardHook_OnKeyboardAction(object sender, WinHooksNET.Events.KeyboardHookEventArgs e)
        {
            if (!run)
            {
                return;
            }
                

            if (e.Key == Key.LeftShift)
            {
                _shouldStop = true;
                run = false;
                this.thread.Abort();
                this.label2.Text = "Zatrzymany";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.run = true;
            this.label2.Text = "Uruchomiony";
            _keyboardHook.OnKeyboardAction += _keyboardHook_OnKeyboardAction;
            
            thread = new Thread(DoMouseClick);
            thread.Start();

            while (!thread.IsAlive);
            Thread.Sleep(1);
            thread.Join();
        }

    }
}
